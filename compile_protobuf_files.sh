#!/bin/bash
if [ $# -ne 2 ]
then
    SCRIPT_NAME=`basename "$0"`
    echo "Usage:"
    echo "    $SCRIPT_NAME <protobuf files path> <compiled files path>"
    echo "Example:"
    echo "    $SCRIPT_NAME ../ta3ta2-api ./proto/"
    exit 1
fi

# Protobuf files to be compiled without the .proto extension
FILES=(core pipeline primitive problem value)

PROTO_FILES_BASE_PATH=$1
OUTPUT_PATH=$2

# Read Operating System for Linux and MacOS compatibility when running 'sed'
OS=`uname`

echo "> .proto files base path: ${PROTO_FILES_BASE_PATH}"
echo "> Compiled files path: ${OUTPUT_PATH}"

for FILE in "${FILES[@]}"
do
  echo "> Compiling file: $PROTO_FILES_BASE_PATH/$FILE.proto"
  python -m grpc_tools.protoc -I$PROTO_FILES_BASE_PATH/ --python_out=$OUTPUT_PATH --grpc_python_out=$OUTPUT_PATH $PROTO_FILES_BASE_PATH/$FILE.proto

  # Fix import for python3
  for F in "${FILES[@]}"
  do
    if [[ "$OS" == 'Linux' ]]; then
      sed -i "s/import ${F}_pb2/from . import ${F}_pb2/" $OUTPUT_PATH/${FILE}_pb2.py
      sed -i "s/import ${F}_pb2/from . import ${F}_pb2/" $OUTPUT_PATH/${FILE}_pb2_grpc.py
    elif [[ "$OS" == 'Darwin' ]]; then
      sed -i ".bkp" "s/import ${F}_pb2/from . import ${F}_pb2/" $OUTPUT_PATH/${FILE}_pb2.py
      sed -i ".bkp" "s/import ${F}_pb2/from . import ${F}_pb2/" $OUTPUT_PATH/${FILE}_pb2_grpc.py
    fi
  done
  rm -f $OUTPUT_PATH/${FILE}_pb2.py.bkp
  rm -f $OUTPUT_PATH/${FILE}_pb2_grpc.py.bkp

done

echo "> Done."
