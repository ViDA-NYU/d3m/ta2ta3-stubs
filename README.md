Data Driven Discovery - Stub Protocol Implementations
=====================================================

This repository contains stub implementations of the TA2-TA3 protocol, extracted from our system.
It can be used to smoketest a TA2 or TA3 system, checking that it speaks the protocol correctly.

### Upgrading this project to new API releases

Clone the `ta3ta2-api` with the latest API and then recompile the protobuf files using the 
script `compile_protobuf_files.sh` provided in this project:

```
$ ./compile_protobuf_files.sh
Usage:
    compile_protobuf_files.sh <protobuf files path> <compiled files path>
Example:
    compile_protobuf_files.sh ../ta3ta2-api ./proto/
```

- `<protobuf files path>` should point to the latest release of the TA3-TA2-API.
- `<compiled files path>` should point to the `./proto` folder in this repository.

### Using dummy result URIs

Set environment variable "results_root" to tell the stub where result files are to be written.
"results_root" must be a directory.
**The entire "results_root" directory will be overwritten by the prediction result files!**
Either make sure that results_root does not exist,
or set environment variable "results_root_overwrite=1" to allow such overwriting.

Dummy result files assume seed dataset "185_baseball".
The feature to be predicted is "Hall_of_Fame".
The prediction column is "Hall_of_Fame_predicted".
