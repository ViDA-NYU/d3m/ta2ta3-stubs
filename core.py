import os
import re
import time
import shutil
import random
import uuid

from google.protobuf.timestamp_pb2 import Timestamp

import proto.core_pb2 as core_pb2
import proto.core_pb2_grpc as core_pb2_grpc
import proto.problem_pb2 as problem_pb2
import proto.value_pb2 as value_pb2

from common import logger, print_msg, __version__

if "D3MOUTPUTDIR" in os.environ:
    D3MOUTPUTDIR = os.environ['D3MOUTPUTDIR']
else:
    D3MOUTPUTDIR = os.path.abspath('./output')

if D3MOUTPUTDIR.endswith("/"):
    D3MOUTPUTDIR = D3MOUTPUTDIR[0:-1] # strip out last character

logger.info("Initializing TA2 stubs server...")
logger.info("D3MOUTPUTDIR=%s" % D3MOUTPUTDIR)

def copy_results(results_root):
    """
    Copy the dummy result files to results_root.
    """
    try:
        shutil.copytree("./data", results_root)
    except OSError:
        overwrite = False
        if "results_root_overwrite" in os.environ:
            overwrite = os.environ["results_root_overwrite"]
        if not overwrite and os.path.exists(results_root):
            logger.info("results_root %s is not empty, result uris will not point to valid files" % results_root)
            logger.info("overwrite it by setting env results_root_overwrite=1")
        else:
            shutil.rmtree(results_root)
            shutil.copytree("./data", results_root)

results_root = D3MOUTPUTDIR + '/predictions'
logger.info("Using following path for prediction results: {}".format(results_root))
copy_results(results_root)

def result_file_uri(filename):
    path = os.path.abspath(os.path.join(results_root, filename))
    if path[0] == '/':
        path = 'file://' + path
    return path

class Core(core_pb2_grpc.CoreServicer):

    def __init__(self):
        self.score_requests = {}
        self.search_ids = set()
        self.solutions = {} # maps search ids to solutions: { search_id : solutions[] }
        self.solutions_ids = []
        self.fitted_solution_ids = []
        self.fit_solution_req_ids = []
        self.produced_solution_ids = []
        self.produce_solution_req_ids = []
        self.prediction_id_index = 0

    def Hello(self, request, context):
        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[
            core_pb2.protocol_version]
        user_agent = "ta2_stub %s" % __version__

        logger.info("Responding Hello! with user_agent=[%s] "
                    "and protocol version=[%s])",
                    user_agent, version)

        return core_pb2.HelloResponse(
            user_agent=user_agent,
            version=version
        )

    def SearchSolutions(self, request, context):
        # Log input request parameters
        logger.info("Received SearchSolutions() call with params:")
        print_msg(request)
        
        # Create and return response message
        search_id = str(uuid.uuid4())
        self.search_ids.add(search_id)
        logger.info("Started search with id=%s", search_id)
        return core_pb2.SearchSolutionsResponse(
            search_id=search_id
        )

    def GetSearchSolutionsResults(self, request, context):

        logger.info("Got GetSearchSolutionsResults request, search_id=%s",
                    request.search_id)

        number_of_solutions = random.randint(5, 15)
        start_time = time.time()
        solutions = create_solutions(number_of_solutions)
        self.solutions_ids.extend([s[0] for s in solutions])

        yield core_pb2.GetSearchSolutionsResultsResponse(
            progress=create_progress_msg(core_pb2.PENDING, start_time),
            done_ticks=0,
            all_ticks=number_of_solutions,
            solution_id="",
            internal_score=float("nan"),
            scores=[]
        )

        done_ticks = 0
        all_ticks = number_of_solutions

        for solution_id, internal_score in solutions:
            done_ticks = done_ticks + 1
            yield core_pb2.GetSearchSolutionsResultsResponse(
                progress=create_progress_msg(core_pb2.RUNNING, start_time),
                done_ticks=done_ticks,
                all_ticks=all_ticks,
                solution_id=solution_id,
                internal_score=internal_score,
                # internal_score=float("nan"),
                scores=[]
            )
            time.sleep(0.5)

        yield core_pb2.GetSearchSolutionsResultsResponse(
            progress=create_progress_msg(core_pb2.COMPLETED, start_time),
            done_ticks=done_ticks,
            all_ticks=all_ticks,
            solution_id="",
            internal_score=float("nan"),
            scores=[]
        )

    def StopSearchSolutions(self, request, context):
        '''
        The TA2 should stop search for new solutions, but it should leave
        all currently found solutions available.
        '''
        logger.info("Stopping solution search for search_id=%s",
                    request.search_id)
        return core_pb2.StopSearchSolutionsResponse()

    def EndSearchSolutions(self, request, context):
        '''
        The TA2 should end the search and releases all resources associated
        with the solution search.
        '''
        logger.info("Ending solution search for search_id=%s",
                    request.search_id)
        self.search_ids.remove(request.search_id)
        return core_pb2.EndSearchSolutionsResponse()

    def DescribeSolution(self, request, context):
        return core_pb2.DescribeSolutionResponse()

    def ScoreSolution(self, request, context):
        logger.info("Requested score for solution solution_id=%s",
                    request.solution_id)
        score_req_id = "score_req_id_%d" % len(self.score_requests)
        
        scores = []
        for problem_metric in request.performance_metrics:
            scores.append(create_score(problem_metric.metric))
        self.score_requests[score_req_id] = scores
        
        return core_pb2.ScoreSolutionResponse(request_id=score_req_id)

    def GetScoreSolutionResults(self, request, context):
        logger.info("Requested GetScoreSolutionResults for request_id=%s",
                    request.request_id)

        scores = self.score_requests[request.request_id]
        
        # Initial state with pending progress status
        results = [(create_progress_msg(core_pb2.PENDING), [])]

        # From the docs: "List can be incomplete while the process is in progress."
        # To simulate partially computed scores, we generate one message for each
        # "partial state", i.e., we add one individual score in the list at a time
        for i in range(len(scores)):
            results.append((create_progress_msg(core_pb2.RUNNING), scores[0 : i+1]))
        
        results.append((create_progress_msg(core_pb2.COMPLETED), scores))

        for progress, score in results:
            # logger.info("Returning score for request_id=%s score=%f metric=%s",
            #         request.request_id, score.value.raw.double, score.metric.metric)
            time.sleep(0.25)
            yield core_pb2.GetScoreSolutionResultsResponse(
                progress=progress,
                scores=score
            )

    def FitSolution(self, request, context):
        logger.info("Fitting solution with id=%s", request.solution_id)
        self.fitted_solution_ids.append(request.solution_id)
        fit_solution_req_id = "fit_solution_req_%d" % len(self.fitted_solution_ids)
        logger.info("Returning FitSolutionResponse with request_id=%s", fit_solution_req_id)
        self.fit_solution_req_ids.append(fit_solution_req_id)
        return core_pb2.FitSolutionResponse(request_id=fit_solution_req_id)

    def GetFitSolutionResults(self, request, context):
        req_index = self.fit_solution_req_ids.index(request.request_id)
        fitted_sol_id = self.fitted_solution_ids[req_index]
        logger.info("Returning GetFitSolutionResultsResponse for fitted solution %s", fitted_sol_id)
        yield core_pb2.GetFitSolutionResultsResponse(
            progress=create_progress_msg(core_pb2.PENDING)
        )
        time.sleep(.5)
        yield core_pb2.GetFitSolutionResultsResponse(
            progress=create_progress_msg(core_pb2.RUNNING)
        )
        time.sleep(.5)
        yield core_pb2.GetFitSolutionResultsResponse(
            fitted_solution_id=fitted_sol_id,
            progress=create_progress_msg(core_pb2.COMPLETED)
        )

    def ProduceSolution(self, request, context):
        logger.info("Producing solution with id=%s", request.fitted_solution_id)
        if request.fitted_solution_id not in self.fitted_solution_ids:
            logger.error("Requested to produce solution that was not fitted before!")
            return None # TODO: what is the expected error response?
        self.produced_solution_ids.append(request.fitted_solution_id)
        produce_solution_req_id = "produce_solution_req_%d" % len(self.produced_solution_ids)
        logger.info("Returning ProduceSolutionResponse with request_id=%s", produce_solution_req_id)
        self.produce_solution_req_ids.append(produce_solution_req_id)
        return core_pb2.ProduceSolutionResponse(request_id=produce_solution_req_id)

    def GetProduceSolutionResults(self, request, context):
        req_index = self.produce_solution_req_ids.index(request.request_id)
        fitted_solution_id = self.produced_solution_ids[req_index]
        logger.info("Returning GetProduceSolutionResultsResponse for fitted solution %s", fitted_solution_id)
        prediction_files = ["predict1.csv", "predict2.csv", "predict3.csv"]
        yield core_pb2.GetProduceSolutionResultsResponse(
            progress=create_progress_msg(core_pb2.PENDING)
        )
        time.sleep(.5)
        yield core_pb2.GetProduceSolutionResultsResponse(
            progress=create_progress_msg(core_pb2.RUNNING)
        )
        time.sleep(.5)
        yield core_pb2.GetProduceSolutionResultsResponse(
            progress=create_progress_msg(core_pb2.COMPLETED),
            exposed_outputs={
                "outputs.0": value_pb2.Value(
                    csv_uri=result_file_uri(prediction_files[(self.prediction_id_index % 3)])
                )
            }
        )
        self.prediction_id_index = self.prediction_id_index + 1

    def SolutionExport(self, request, context):
        solution_id = request.solution_id
        rank = request.rank
        if solution_id not in self.solutions_ids:
            logger.warn("Requested to export solution that was not created before!")
        else:
            logger.info("Exporting solution result for solution_id=%s and rank=%d",
                        solution_id, rank)
            
            # create directory if absent
            export_path = D3MOUTPUTDIR + '/pipelines_ranked'
            if not os.path.isdir(export_path):
                os.mkdir(export_path)
            
            # write pipeline file
            pipeline_path = '{}/{}.json'.format(export_path, solution_id)
            print('Writing pipeline descrition at {}'.format(pipeline_path))
            with open(pipeline_path, 'w') as f:
                f.write(str(rank))
                f.write(' ')
                f.write(solution_id)
                f.write('\n')
            
        return core_pb2.SolutionExportResponse()

    def UpdateProblem(self, request, context):
        pass # TODO

    def ListPrimitives(self, request, context):
        pass # TODO

    def SaveSolution(self, request, context):
        pass # TODO

    def LoadSolution(self, request, context):
        pass # TODO

    def SaveFittedSolution(self, request, context):
        pass # TODO

    def LoadFittedSolution(self, request, context):
        pass # TODO

def create_solutions(number_of_solutions):
    solutions = []
    for i in range(number_of_solutions):
        # solution_id, internal_score
        solutions.append((str(uuid.uuid4()), random.random())),
    return solutions

def create_progress_msg(progress_state, start_time = time.time()):

    def status_msg(progress_state):
        return {
            core_pb2.PENDING: 'Waiting to be initialized...',
            core_pb2.RUNNING: 'Running...',
            core_pb2.COMPLETED: 'Completed'
        }.get(progress_state, core_pb2.PENDING)  # default value is pending

    def protobuf_timestamp(t):
        seconds = int(t)
        nanos = int((t - seconds) * 10 ** 9)
        return Timestamp(seconds=seconds, nanos=nanos)

    status = status_msg(progress_state)

    if progress_state == core_pb2.RUNNING:
        start = protobuf_timestamp(t=start_time)
        end = None
    elif progress_state == core_pb2.COMPLETED:
        now = time.time()
        start = protobuf_timestamp(t=start_time)
        end = protobuf_timestamp(t=now)
    else:
        start = None
        end = None

    return core_pb2.Progress(
        state=progress_state,
        status=status,
        start=start,
        end=end
    )

def generate_random_score(metric):
    if metric == problem_pb2.MEAN_SQUARED_ERROR:
        return random.random() * 100;
    elif metric == problem_pb2.ROOT_MEAN_SQUARED_ERROR or \
         metric == problem_pb2.ROOT_MEAN_SQUARED_ERROR_AVG:
        return random.random() * 10;
    else:
        return random.random();

def create_score(metric):
    score = generate_random_score(metric)
    return core_pb2.Score(
        metric=problem_pb2.ProblemPerformanceMetric(
            metric=metric,
            k=1,
            pos_label=''),
        value=value_pb2.Value(
            raw=value_pb2.ValueRaw(double=score)
        ),
    )