from __future__ import absolute_import, division, print_function

import grpc
import logging
import sys

import proto.core_pb2 as core_pb2
import proto.core_pb2_grpc as core_pb2_grpc
import proto.problem_pb2 as problem_pb2
import proto.value_pb2 as value_pb2

from common import print_msg, __version__

def main():
    logging.basicConfig(level=logging.INFO)

    if len(sys.argv) > 1:
        address = sys.argv[1]
    else:
        address = 'localhost:45042'

    channel = grpc.insecure_channel(address)
    stub = core_pb2_grpc.CoreStub(channel)

    version = core_pb2.DESCRIPTOR.GetOptions().Extensions[
        core_pb2.protocol_version]

    #
    # rpc Hello()
    #
    print('\n> Saying Hello() to TA2 server...')
    hello_response = stub.Hello(core_pb2.HelloRequest())
    assert hello_response != None
    print('  TA2 server responded with: user_name=[{}] version=[{}]'.format(
        hello_response.user_agent,
        hello_response.version
    ))
    print_msg(hello_response)

    #
    # rpc SearchSolutions()
    #
    print('\n> Calling SearchSolutions()...')

    metric_f1_macro = problem_pb2.ProblemPerformanceMetric(
        metric=problem_pb2.F1_MACRO
    )
    metric_f1_micro = problem_pb2.ProblemPerformanceMetric(
        metric=problem_pb2.F1_MICRO
    )

    problem = problem_pb2.Problem(
        id='185_baseball_problem', # Deprecated. Moved to ProblemDescription message.
        name='baseball_problem',   # Deprecated. Moved to ProblemDescription message.
        version='1.0',             # Deprecated. Moved to ProblemDescription message.
        description='Debugging baseball problem', # Deprecated. Moved to ProblemDescription message.
        task_type=problem_pb2.CLASSIFICATION,
        task_subtype=problem_pb2.MULTICLASS,
        performance_metrics=[
            metric_f1_macro
        ]
    )

    problem_description = problem_pb2.ProblemDescription(
        problem=problem,
        id='185_baseball_problem',
        name='baseball_problem',
        version='1.0',
        description='Debugging baseball problem',
    )

    input_dataset = value_pb2.Value(
        dataset_uri='file:///data/dataset1/datasetDoc.json'
    )
    reply = stub.SearchSolutions(core_pb2.SearchSolutionsRequest(
        user_agent='text_client %s' % __version__,
        version=version,
        #time_bound=,
        #priority=,
        #allowed_value_types=,
        problem=problem_description,
        #template=, # PipelineDescription
        inputs=[
            input_dataset
        ]
    ))
    search_id = reply.search_id
    print('  Started search with search_id={}'.format(search_id))
    print_msg(reply)

    #
    # rpc GetSearchSolutionsResults()
    #
    print('\n> Calling GetSearchSolutionsResults() for search_id={}'.format(search_id))
    results_stream = stub.GetSearchSolutionsResults(core_pb2.GetSearchSolutionsResultsRequest(
        search_id=search_id
    ))

    print('\n> Receiving search solution results:')
    solutions = set()
    for solution_result in results_stream:
        if solution_result.solution_id == "":
            continue
        print('  id={} state={} status=[{}]'.format(
            solution_result.solution_id,
            solution_result.progress.state,
            solution_result.progress.status))
        print_msg(solution_result)
        solutions.add(solution_result.solution_id)
    print(solutions)

    #
    # rpc GetSearchSolutionsResults()
    #
    print('\n> Calling StopSearchSolutions() for search_id={}'.format(search_id))
    stop_response = stub.StopSearchSolutions(core_pb2.StopSearchSolutionsRequest(
        search_id=search_id
    ))
    assert stop_response != None

    #
    # rpc ScoreSolution()
    #
    score_request_ids = []
    for solution_id in solutions:
        print('\n> Requesting score for solution_id={}'.format(solution_id))
        score_solution_response = stub.ScoreSolution(core_pb2.ScoreSolutionRequest(
            solution_id=solution_id,
            inputs=[input_dataset],
            performance_metrics=[
                metric_f1_macro,
                metric_f1_micro
            ]
            #users=,
            #configuration=, # ScoringConfiguration
        ))
        assert score_solution_response != None
        print_msg(score_solution_response)
        score_request_ids.append(score_solution_response.request_id)

    #
    # rpc ScoreSolution()
    #
    for score_req_id in score_request_ids:
        print('\n> Requesting score results for score_req_id={}'.format(score_req_id))
        score_result_stream = stub.GetScoreSolutionResults(
            core_pb2.GetScoreSolutionResultsRequest(request_id=score_req_id)
        )
        assert score_result_stream != None
        for score_result in score_result_stream:
            print_msg(score_result)

    #
    # rpc GetSearchSolutionsResults()
    #
    print('\n> Calling EndSearchSolutions() for search_id={}'.format(search_id))
    end_response = stub.EndSearchSolutions(core_pb2.EndSearchSolutionsRequest(
        search_id=search_id
    ))
    assert end_response != None

    #
    # rpc FitSolution()
    # Here we only fit "solution_0".
    #
    fitted_solution_id = "solution_0"
    print('\n> Calling FitSolution() for solution=%s' % fitted_solution_id)
    end_response = stub.FitSolution(core_pb2.FitSolutionRequest(
        solution_id=fitted_solution_id
    ))
    assert end_response != None
    print_msg(end_response)
    fit_solution_req_id = end_response.request_id

    #
    # rpc GetFitSolutionResultsRequest()
    #
    results_stream = stub.GetFitSolutionResults(core_pb2.GetFitSolutionResultsRequest(
        request_id=fit_solution_req_id
    ))
    print('\n> Receiving fit solution results for fit solution request %s' % fit_solution_req_id)
    for fit_solution_result in results_stream:
        print_msg(fit_solution_result)

    #
    # rpc ProduceSolution()
    # Here we only produce "solution_0".
    #
    print('\n> Calling ProduceSolution() for solution=%s' % fitted_solution_id)
    end_response = stub.ProduceSolution(core_pb2.ProduceSolutionRequest(
        fitted_solution_id=fitted_solution_id,
        inputs=[input_dataset],
        expose_outputs=["outputs.0"]
    ))
    assert end_response != None
    print_msg(end_response)
    produce_solution_req_id = end_response.request_id

    #
    # rpc GetProduceSolutionResultsRequest()
    #
    results_stream = stub.GetProduceSolutionResults(core_pb2.GetProduceSolutionResultsRequest(
        request_id=produce_solution_req_id
    ))
    print('\n > Receiving produce solution results for produce solution request %s' % produce_solution_req_id)
    for produce_solution_result in results_stream:
        print_msg(produce_solution_result)


    rank = 0
    for solution_id in solutions:
        print('\n> Calling SolutionExport() for solution={}'.format(solution_id))
        stub.SolutionExport(core_pb2.SolutionExportRequest(
            solution_id=solution_id,
            rank=rank
        ))
        rank = rank + 1


if __name__ == '__main__':
    main()
